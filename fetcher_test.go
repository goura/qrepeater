package qrepeater

import (
	"strconv"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

type MockedFetcherTransport struct {
	mock.Mock
	Number int
}

func (mt *MockedFetcherTransport) GetMessages() ([]*Message, error) {
	genMsg := func(num int) *Message {
		return &Message{
			Body: strconv.Itoa(num),
		}
	}
	mt.Number += 1
	n1 := mt.Number
	mt.Number += 1
	n2 := mt.Number
	return []*Message{genMsg(n1), genMsg(n2)}, nil
}

func (mt *MockedFetcherTransport) DeleteMessage(string) error {
	return nil
}

func TestFetcher(t *testing.T) {
	dt := &MockedFetcherTransport{}
	dt.Number = 0

	f := Fetcher{
		Transport:  dt,
		NumWorkers: 2,
	}

	err := f.Start()
	assert.Nil(t, err)

	count := 0
	for msg := range f.MsgC {
		count += 1
		_ = msg.Body
		if count > 9 {
			break
		}
	}
	err = f.Stop()
	assert.Nil(t, err)
}
