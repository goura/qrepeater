package main

import (
	"fmt"
	"os"

	"bitbucket.org/goura/qrepeater/repeater"
)

func main() {
	err := repeater.RunRepeater()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}
