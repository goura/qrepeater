package qrepeater

import (
	"strconv"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

type MockedMessageFinisher struct {
	mock.Mock
}

func (mf *MockedMessageFinisher) FinishMessage(FinishHandle string) error {
	args := mf.Called(FinishHandle)
	return args.Error(0)
}

type MockedFeederTransport struct {
	mock.Mock
}

func (mt *MockedFeederTransport) PutMessage(m *Message) error {
	args := mt.Called(m)
	return args.Error(0)
}

func TestFeeder(t *testing.T) {
	mt := &MockedFeederTransport{}
	mf := &MockedMessageFinisher{}

	// Setup fixtures and expectations
	testData := mt.TestData()

	for i := 0; i < 10; i++ {
		// Create dummy message
		numStr := strconv.Itoa(i)
		m := &Message{
			Body:         numStr,
			FinishHandle: numStr,
			Finisher:     mf,
		}
		testData[numStr] = m

		// Setup expectation
		mt.On("PutMessage", m).Return(nil)
		mf.On("FinishMessage", m.FinishHandle).Return(nil)
	}

	f := Feeder{
		Transport:  mt,
		NumWorkers: 10,
	}

	err := f.Start()
	assert.Nil(t, err)

	for _, m := range testData {
		f.MsgC <- m.(*Message)
	}

	err = f.Stop()
	assert.Nil(t, err)

	// Assertion
	mt.AssertNumberOfCalls(t, "PutMessage", 10)
	mf.AssertNumberOfCalls(t, "FinishMessage", 10)
}
