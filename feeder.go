package qrepeater

import (
	"sync"

	log "github.com/Sirupsen/logrus"
)

// FeederTransport implements the actual part to put a message into a queue.
type FeederTransport interface {
	PutMessage(*Message) error
}

// Feeder continuously reads messages from MsgC, and feeds that messages to a queue,
// using Transport(FeederTransport).
type Feeder struct {
	Transport  FeederTransport
	NumWorkers int
	MsgC       chan *Message
	// Internal chan to use to notify workers to stop
	stopC chan interface{}
	// WaitGroup for workers
	wg sync.WaitGroup
}

// Start starts the Feeder.
func (f *Feeder) Start() error {
	f.MsgC = make(chan *Message)
	f.stopC = make(chan interface{})
	return f.FeedMessages()
}

// Start stops the Feeder.
func (f *Feeder) Stop() error {
	defer close(f.MsgC)

	close(f.stopC)
	f.wg.Wait()
	return nil
}

func (f *Feeder) FeedMessages() error {
	// Worker function
	spawn := func() {
		defer f.wg.Done()
		for {
			select {
			case <-f.stopC:
				// f.stopC was closed.
				// This means someone wants this function to stop.
				return
			case m := <-f.MsgC:
				// Put received message and finish(delete) it
				err := f.Transport.PutMessage(m)
				if err != nil {
					log.WithFields(log.Fields{
						"component": "feeder",
						"action":    "put_message",
						"error":     err.Error(),
					}).Info("putting message failed (recoverable failure)")
					continue
				}

				err = m.Finish()
				if err != nil {
					log.WithFields(log.Fields{
						"component": "feeder",
						"action":    "finish_message",
						"error":     err.Error(),
					}).Info("finishing message failed (recoverable failure)")
					continue
				}
			}
		}
	}

	// Spawn workers
	f.wg.Add(f.NumWorkers)
	for i := 0; i < f.NumWorkers; i++ {
		go spawn()
	}

	return nil
}
