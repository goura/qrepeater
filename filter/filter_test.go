package filter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegisterLookup(t *testing.T) {
	// Put two dummy filters
	f1 := Filter{Name: "f1"}
	f2 := Filter{Name: "f2"}

	// Register them
	Register(f1)
	Register(f2)

	// Get the two filters and one non-exsistent filter
	r1, ok1 := Lookup("f1")
	r2, ok2 := Lookup("f2")
	_, ok3 := Lookup("f3")

	// Assert that the two exists and one does not exist
	assert.Equal(t, r1.Name, "f1")
	assert.True(t, ok1)

	assert.Equal(t, r2.Name, "f2")
	assert.True(t, ok2)

	assert.False(t, ok3)
}
