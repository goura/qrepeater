package filter

import (
	"fmt"

	"bitbucket.org/goura/qrepeater"
)

// FilterFunc is a filter function which will be applied to each
// messages before sending it to the output queue.
// Returning nil will prevent the message to be forwarded to the output queue,
// For typical cases, the implementer need to call Message.Finish() manually
// inside the function to prevent the message to be re-appearing in the input queue.
type FilterFunc func(*qrepeater.Message) (*qrepeater.Message, error)

// Filter is a holder for FilterFunc.
// Name will be used by the user to get the filter by name.
type Filter struct {
	FilterFunc FilterFunc
	Name       string
}

var filterMap = map[string]Filter{}

// Register registers a filter by it's Name to the global filterMap.
func Register(filter Filter) error {
	if filter.Name == "" {
		return fmt.Errorf("Filter needs a name")
	}
	filterMap[filter.Name] = filter
	return nil
}

// LookupFilter returns a filter by it's name.
func Lookup(name string) (Filter, bool) {
	filter, ok := filterMap[name]
	return filter, ok
}
