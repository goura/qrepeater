package qrepeater

import (
	"sync"

	log "github.com/Sirupsen/logrus"
)

// FetcherTransport implements the actual part to get a message from a queue.
type FetcherTransport interface {
	GetMessages() ([]*Message, error)
}

// Fetcher continuously gets messages from a queue, and feeds it into MsgC,
// using Transport(FetcherTransport).
type Fetcher struct {
	Transport  FetcherTransport
	NumWorkers int
	MsgC       chan *Message
	// Internal chan to use to notify workers to stop
	stopC chan interface{}
	// WaitGroup for workers
	wg sync.WaitGroup
}

// Start starts the Fetcher.
func (f *Fetcher) Start() error {
	f.MsgC = make(chan *Message, f.NumWorkers*16)
	f.stopC = make(chan interface{})
	return f.FetchMessages()
}

// Stop stops the Fetcher.
func (f *Fetcher) Stop() error {
	defer close(f.MsgC)

	close(f.stopC)
	f.wg.Wait()
	return nil
}

func (f *Fetcher) FetchMessages() error {
	// Worker function
	spawn := func() {
		defer f.wg.Done()
		for {
			ms, err := f.Transport.GetMessages()
			if err != nil {
				log.WithFields(log.Fields{
					"component": "fetcher",
					"action":    "get_messages",
					"error":     err.Error(),
				}).Info("getting messages failed (recoverable failure)")
				continue
			}
			// If f.stopC is closed, return.
			// f.stopC is used to notify this function to end from outside.
			select {
			case <-f.stopC:
				return
			default:
				for _, m := range ms {
					f.MsgC <- m
				}
			}
		}
	}

	// Spawn workers
	f.wg.Add(f.NumWorkers)
	for i := 0; i < f.NumWorkers; i++ {
		go spawn()
	}

	return nil
}
