qrepeater is a cloud queue repeater, which reads a message from one queue, and then then write it to another.

Currently it supports:

- Reading from Amazon SQS
- Writing to Amazon SQS/SNS

User can implement their own filtering function to filter or transform messages before writing them to the destination queue.

Messages will be deleted from the source after it is delivered to the destination.

Motivation
===========

When migrating a system to a new architecture, sometimes we have to deal with small message format changes.

I wanted something that we can use in a case like this.

For example, when there is an old system which posts raw SQS messages and reads raw SQS messages, and we decided to deploy a new system which posts SNS messages and read SQS messages, we have to deal with the SNS envelope, because the new system does not expect raw SQS messages while the old system does not wrap their messages in SNS envelopes.

One strategy might be to change the old system's code to wrap the messages with SNS envelope, or post messages to SNS instead of SQS, but it's not a good idea to touch working systems.

Another strategy might be to change the new system's code to accept raw SQS messages as well as SNS messages, but raw SQS message format will be deprecated as soon as the deployment succeeds, and we don't want to write such a short-life code.

If we can easily have a worker which reads from the old system's SQS queue and forwards them to the new system's SNS topic, we can solve this problem.
