package qrepeater

// MessageFinisher is attached to a message.
// After the receiver receives the message and stored it successfully,
// it will delete the message from the original source
// by calling FinishMessage().
type MessageFinisher interface {
	FinishMessage(FinishHandle string) error
}

type Message struct {
	MessageID    string
	Body         string
	FinishHandle string
	Finisher     MessageFinisher
}

func (m *Message) Finish() error {
	if m.Finisher == nil {
		return nil
	}
	return m.Finisher.FinishMessage(m.FinishHandle)
}
