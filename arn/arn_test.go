package arn

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestARNType1(t *testing.T) {
	s := "arn:aws:service:region:account:resource"
	arn, err := Parse(s)
	assert.Nil(t, err)

	assert.Equal(t, "aws", arn.Partition)
	assert.Equal(t, "service", arn.Service)
	assert.Equal(t, "region", arn.Region)
	assert.Equal(t, "account", arn.Account)
	assert.Equal(t, "resource", arn.Resource)
}

func TestARNType2(t *testing.T) {
	s := "arn:aws:service:region:account:resourcetype/resource"
	arn, err := Parse(s)
	assert.Nil(t, err)

	assert.Equal(t, "aws", arn.Partition)
	assert.Equal(t, "service", arn.Service)
	assert.Equal(t, "region", arn.Region)
	assert.Equal(t, "account", arn.Account)
	assert.Equal(t, "resourcetype", arn.ResourceType)
	assert.Equal(t, "resource", arn.Resource)
}

func TestARNType3(t *testing.T) {
	s := "arn:aws:service:region:account:resourcetype:resource"
	arn, err := Parse(s)
	assert.Nil(t, err)

	assert.Equal(t, "aws", arn.Partition)
	assert.Equal(t, "service", arn.Service)
	assert.Equal(t, "region", arn.Region)
	assert.Equal(t, "account", arn.Account)
	assert.Equal(t, "resourcetype", arn.ResourceType)
	assert.Equal(t, "resource", arn.Resource)
}

func TestARNError(t *testing.T) {
	errorCases := []string{
		"arn:aaaaaaaaa",
		"arn:awss:service:region:account:resource",
		"arm:aws:service:region:account:resource",
	}
	for _, s := range errorCases {
		_, err := Parse(s)
		assert.NotNil(t, err)
	}
}
