// The arn package contains a parser to parse ARNs.
// See: http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html
package arn

import (
	"errors"
	"strings"
)

// ARN type is a representation of an ARN.
type ARN struct {
	Partition    string
	Service      string
	Region       string
	Account      string
	ResourceType string
	Resource     string
}

// Parse takes a string of ARN and parses it into an ARN struct.
func Parse(rawArn string) (arn *ARN, err error) {
	fields := strings.Split(rawArn, ":")
	if !(len(fields) == 6 || len(fields) == 7) {
		return nil, errors.New("Wrong ARN format (wrong number of fields)")
	}
	if fields[0] != "arn" {
		return nil, errors.New("Wrong ARN format (not starting with 'arn')")
	}
	if !(fields[1] == "aws" || fields[1] == "aws-cn") {
		return nil, errors.New("Wrong ARN format (partition not 'aws' or 'aws-cn')")
	}
	arn = &ARN{}
	arn.Partition = fields[1]
	arn.Service = fields[2]
	arn.Region = fields[3]
	arn.Account = fields[4]
	// We accept 3 types of ARNs:
	// Type 1: arn:partition:service:region:account:resourcetype:resource
	// Type 2: arn:partition:service:region:account:resource
	// Type 3: arn:partition:service:region:account:resourcetype/resource
	if len(fields) == 7 {
		// Type 1
		arn.ResourceType = fields[5]
		arn.Resource = fields[6]
		return arn, nil
	}

	// Type 2 or 3
	subFields := strings.Split(fields[5], "/")
	if len(subFields) == 2 {
		// Type 3
		arn.ResourceType = subFields[0]
		arn.Resource = subFields[1]
	} else {
		// Type 2
		arn.Resource = fields[5]
	}
	return arn, nil
}
