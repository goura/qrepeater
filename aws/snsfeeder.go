package aws

import (
	"bitbucket.org/goura/qrepeater"
	"bitbucket.org/goura/qrepeater/arn"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
)

// SNSFeederTransport is type which implements FeederTransport for SNS topics.
type SNSFeederTransport struct {
	AWSConfig *aws.Config
	TopicARN  string
}

// messageToSNSMessage converts a Message into an sns.PublishInput.
func (ft *SNSFeederTransport) messageToSNSMessage(m *qrepeater.Message) (*sns.PublishInput, error) {
	pi := &sns.PublishInput{
		Message:  aws.String(m.Body),
		TopicARN: aws.String(ft.TopicARN),
	}
	return pi, nil
}

// PutMessage publishes a Message to an SNS topic.
func (ft *SNSFeederTransport) PutMessage(m *qrepeater.Message) error {
	svc := sns.New(ft.AWSConfig)
	params, err := ft.messageToSNSMessage(m)
	if err != nil {
		return err
	}
	_, err = svc.Publish(params)
	return err
}

// NewSNSFeeder creates an Feeder, equipped with SNSFeederTransport,
// to post to a given SNS topic.
// NumWorkers will be set to 1.
func NewSNSFeeder(topicRawARN string) (*qrepeater.Feeder, error) {
	arn, err := arn.Parse(topicRawARN)
	if err != nil {
		return nil, err
	}
	config := &aws.Config{
		Region: arn.Region,
	}

	ft := &SNSFeederTransport{
		AWSConfig: config,
		TopicARN:  topicRawARN,
	}
	f := qrepeater.Feeder{
		Transport:  ft,
		NumWorkers: 1,
	}
	return &f, nil
}
