// +build integration

package aws

import (
	"errors"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/goura/qrepeater"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func CreateTempSNSTopic() (string, error) {
	svc := sns.New(nil)

	unixtime := time.Now().Unix()
	randNum := rand.Uint32()
	topicName := fmt.Sprintf("test-%d-%d", unixtime, randNum)
	params := &sns.CreateTopicInput{
		Name: aws.String(topicName),
	}

	resp, err := svc.CreateTopic(params)

	if err != nil {
		return "", err
	}
	return *resp.TopicARN, nil
}

func DeleteTempSNSTopic(TopicARN string) error {
	svc := sns.New(nil)

	params := &sns.DeleteTopicInput{
		TopicARN: aws.String(TopicARN),
	}

	_, err := svc.DeleteTopic(params)
	return err
}

func QueueARN(queueURL string) (string, error) {
	svc := sqs.New(nil)

	s := "All"
	params := &sqs.GetQueueAttributesInput{
		AttributeNames: []*string{&s},
		QueueURL:       aws.String(queueURL),
	}
	resp, err := svc.GetQueueAttributes(params)
	if err != nil {
		return "", err
	}
	arn, ok := resp.Attributes["QueueArn"]
	if !ok {
		return "", errors.New("QueueARN not found in attributes")
	}
	return *arn, nil
}

func SubscribeQueueToTopic(queueARN, topicARN string) error {
	svc := sns.New(nil)

	params := &sns.SubscribeInput{
		Endpoint: aws.String(queueARN),
		Protocol: aws.String("sqs"),
		TopicARN: aws.String(topicARN),
	}
	_, err := svc.Subscribe(params)
	return err
}

type SNSFeederTestSuite struct {
	suite.Suite
	QueueURL string
	TopicARN string
}

func (suite *SNSFeederTestSuite) SetupTest() {
	var err error
	suite.TopicARN, err = CreateTempSNSTopic()
	assert.Nil(suite.T(), err)
	suite.T().Logf("TopicARN: %s", suite.TopicARN)

	suite.QueueURL, err = CreateTempSQSQueue()
	assert.Nil(suite.T(), err)
	suite.T().Logf("QueueURL: %s", suite.QueueURL)

	queueARN, err := QueueARN(suite.QueueURL)
	assert.Nil(suite.T(), err)
	suite.T().Logf("queueARN: %s", queueARN)

	err = SubscribeQueueToTopic(queueARN, suite.TopicARN)
	assert.Nil(suite.T(), err)
}

func (suite *SNSFeederTestSuite) TearDownTest() {
	var err error
	if suite.QueueURL != "" {
		err = DeleteTempSQSQueue(suite.QueueURL)
	}
	if suite.TopicARN != "" {
		err = DeleteTempSNSTopic(suite.TopicARN)
	}
	assert.Nil(suite.T(), err)
}

func (suite *SNSFeederTestSuite) TestPutMessage() {
	// Put a message, and ensure there's no error.
	transport := SNSFeederTransport{
		TopicARN: suite.TopicARN,
	}
	msg := &qrepeater.Message{
		Body: "hello",
	}
	err := transport.PutMessage(msg)
	assert.Nil(suite.T(), err)
}

func TestSNSFeederTestSuite(t *testing.T) {
	suite.Run(t, new(SNSFeederTestSuite))
}
