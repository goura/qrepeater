package aws

import (
	"bitbucket.org/goura/qrepeater"
	"bitbucket.org/goura/qrepeater/arn"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

// SQSFetcherTransport implements FetcherTransport interface and
// fetches messages from SQS.
type SQSFetcherTransport struct {
	AWSConfig           *aws.Config
	QueueURL            string
	MaxNumberOfMessages int64
	WaitTimeSeconds     int64
}

// sqsReceiveMessages receives messages from an SQS queue.
func (ft *SQSFetcherTransport) sqsReceiveMessages() ([]*sqs.Message, error) {
	svc := sqs.New(ft.AWSConfig)
	params := &sqs.ReceiveMessageInput{
		QueueURL: aws.String(ft.QueueURL),
		AttributeNames: []*string{
			aws.String("QueueAttributeName"),
		},
		MaxNumberOfMessages: aws.Long(ft.MaxNumberOfMessages),
		MessageAttributeNames: []*string{
			aws.String("MessageAttributeName"),
		},
		WaitTimeSeconds: aws.Long(ft.WaitTimeSeconds),
	}
	resp, err := svc.ReceiveMessage(params)

	// Handle error case
	if err != nil {
		return nil, err
	}

	// resp.Messages is []*sqs.Message
	return resp.Messages, nil
}

// sqsDeleteMessage deletes message from an SQS queue.
func (ft *SQSFetcherTransport) sqsDeleteMessage(ReceiptHandle string) error {
	svc := sqs.New(ft.AWSConfig)
	params := &sqs.DeleteMessageInput{
		QueueURL:      aws.String(ft.QueueURL),
		ReceiptHandle: aws.String(ReceiptHandle),
	}
	_, err := svc.DeleteMessage(params)

	// Handle error case
	return err
}

// sqsMessagesToMessages converts sqs.Messages into qrepeater.Messages.
func (ft *SQSFetcherTransport) sqsMessagesToMessages(sqsMsgs []*sqs.Message) ([]*qrepeater.Message, error) {
	msgs := []*qrepeater.Message{}
	for _, sqsMsg := range sqsMsgs {
		msg := &qrepeater.Message{
			Body:         *sqsMsg.Body,
			FinishHandle: *sqsMsg.ReceiptHandle,
			Finisher:     ft,
		}
		msgs = append(msgs, msg)
	}
	return msgs, nil
}

// GetMessage gets messages from an SQS queue.
func (ft *SQSFetcherTransport) GetMessages() ([]*qrepeater.Message, error) {
	sqsMsgs, err := ft.sqsReceiveMessages()
	if err != nil {
		return []*qrepeater.Message{}, err
	}

	return ft.sqsMessagesToMessages(sqsMsgs)
}

// FinishMessage implements MessageFinisher interface, actually deletes
// a message from an SQS queue.
func (ft *SQSFetcherTransport) FinishMessage(FinishHandle string) error {
	return ft.sqsDeleteMessage(FinishHandle)
}

// NewSQSFetcher creates a new Fetcher equipped with SQSFetcherTransport,
// which fetchers messages from an SQS queue. NumWorkers will be set to 1.
func NewSQSFetcher(queueARN string) (*qrepeater.Fetcher, error) {
	arn, err := arn.Parse(queueARN)
	if err != nil {
		return nil, err
	}
	config := &aws.Config{
		Region: arn.Region,
	}

	queueURL, err := QueueARNtoURL(queueARN)
	if err != nil {
		return nil, err
	}

	ft := &SQSFetcherTransport{
		AWSConfig:           config,
		QueueURL:            queueURL,
		WaitTimeSeconds:     10,
		MaxNumberOfMessages: 10,
	}
	f := qrepeater.Fetcher{
		Transport:  ft,
		NumWorkers: 1,
	}
	return &f, nil
}
