// +build integration

package aws

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/goura/qrepeater"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// CreateTempSQSQueue creates a temporary SQS queue and returns its URL.
func CreateTempSQSQueue() (string, error) {
	svc := sqs.New(nil)

	unixtime := time.Now().Unix()
	randNum := rand.Uint32()
	queueName := fmt.Sprintf("test-%d-%d", unixtime, randNum)
	params := &sqs.CreateQueueInput{
		QueueName: aws.String(queueName),
	}

	resp, err := svc.CreateQueue(params)

	if err != nil {
		return "", err
	}
	return *resp.QueueURL, nil
}

// DeleteTempSQSQueue deletes an SQS queue corresponding the given URL.
func DeleteTempSQSQueue(QueueURL string) error {
	svc := sqs.New(nil)

	params := &sqs.DeleteQueueInput{
		QueueURL: aws.String(QueueURL),
	}

	_, err := svc.DeleteQueue(params)
	return err
}

type SQSFeederTestSuite struct {
	suite.Suite
	QueueURL string
}

func (suite *SQSFeederTestSuite) SetupTest() {
	var err error
	suite.QueueURL, err = CreateTempSQSQueue()
	assert.Nil(suite.T(), err)
	suite.T().Logf("QueueURL: %s", suite.QueueURL)
}

func (suite *SQSFeederTestSuite) TearDownTest() {
	var err error
	if suite.QueueURL != "" {
		err = DeleteTempSQSQueue(suite.QueueURL)
	}
	assert.Nil(suite.T(), err)
}

func (suite *SQSFeederTestSuite) TestPutMessage() {
	// Put a message, and ensure there's no error.
	transport := SQSFeederTransport{
		QueueURL: suite.QueueURL,
	}
	msg := &qrepeater.Message{
		Body: "hello",
	}
	err := transport.PutMessage(msg)
	assert.Nil(suite.T(), err)
}

func TestSQSFeederTestSuite(t *testing.T) {
	suite.Run(t, new(SQSFeederTestSuite))
}
