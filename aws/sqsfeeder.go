package aws

import (
	"fmt"

	"bitbucket.org/goura/qrepeater"
	"bitbucket.org/goura/qrepeater/arn"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

// SQSFeederTransport type implements FeederTransport which posts message to SQS.
type SQSFeederTransport struct {
	AWSConfig    *aws.Config
	QueueURL     string
	DelaySeconds int64
}

// messageToSQSMessage converts a Message into sqs.SendMessageInput.
func (ft *SQSFeederTransport) messageToSQSMessage(m *qrepeater.Message) (*sqs.SendMessageInput, error) {
	sqsMsg := &sqs.SendMessageInput{
		MessageBody:  aws.String(m.Body),
		QueueURL:     aws.String(ft.QueueURL),
		DelaySeconds: aws.Long(ft.DelaySeconds),
	}
	return sqsMsg, nil
}

// PutMessage posts a message to SQS.
func (ft *SQSFeederTransport) PutMessage(m *qrepeater.Message) error {
	svc := sqs.New(ft.AWSConfig)
	params, err := ft.messageToSQSMessage(m)
	if err != nil {
		return err
	}
	_, err = svc.SendMessage(params)
	return err
}

// QueueARNtoURL converts an SQS queue ARN into an SQS queue URL.
func QueueARNtoURL(queueARN string) (string, error) {
	arn, err := arn.Parse(queueARN)
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf("https://sqs.%s.amazonaws.com/%s/%s", arn.Region, arn.Account, arn.Resource)
	return url, nil
}

// NewSQSFeeder creates a new Feeder equipped with SQSFeederTransport
// which posts messages to a given SQS queue.
func NewSQSFeeder(queueARN string) (*qrepeater.Feeder, error) {
	arn, err := arn.Parse(queueARN)
	if err != nil {
		return nil, err
	}

	config := &aws.Config{
		Region: arn.Region,
	}

	queueURL, err := QueueARNtoURL(queueARN)
	if err != nil {
		return nil, err
	}

	ft := &SQSFeederTransport{
		AWSConfig:    config,
		QueueURL:     queueURL,
		DelaySeconds: 0,
	}
	f := qrepeater.Feeder{
		Transport:  ft,
		NumWorkers: 1,
	}
	return &f, nil
}
