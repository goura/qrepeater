// +build integration

package aws

import (
	"fmt"
	"net/url"
	"strings"
	"testing"
	"time"

	"bitbucket.org/goura/qrepeater"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// QueueURLtoARN converts an SQS queue URL to an SQS queue ARN
// only by text matching.
func QueueURLtoARN(queueURL string) (string, error) {
	// from | https://sqs.ap-northeast-1.amazonaws.com/12345678901/aaaa
	// to   | arn:aws:sqs:ap-northeast-1:12345678901:aaaa
	u, err := url.Parse(queueURL)
	if err != nil {
		return "", err
	}

	fields := strings.Split(u.Host, ".")
	if len(fields) < 3 {
		return "", fmt.Errorf("Wrong SQS queue URL: %s", queueURL)
	}
	region := fields[1]

	fields = strings.Split(u.Path, "/")
	if len(fields) != 3 {
		return "", fmt.Errorf("Wrong SQS queue URL: %s", queueURL)
	}
	account := fields[1]
	resource := fields[2]

	arn := fmt.Sprintf("arn:aws:sqs:%s:%s:%s", region, account, resource)
	return arn, nil
}

func TestQueueURLtoARN(t *testing.T) {
	s1 := "https://sqs.ap-northeast-1.amazonaws.com/12345678901/aaaa"
	s2 := "arn:aws:sqs:ap-northeast-1:12345678901:aaaa"
	result, err := QueueURLtoARN(s1)
	assert.Nil(t, err)
	assert.Equal(t, s2, result)
}

type SQSFetcherTestSuite struct {
	suite.Suite
	QueueURL string
}

func (suite *SQSFetcherTestSuite) SetupTest() {
	var err error
	suite.QueueURL, err = CreateTempSQSQueue()
	assert.Nil(suite.T(), err)
	suite.T().Logf("QueueURL: %s", suite.QueueURL)
}

func (suite *SQSFetcherTestSuite) TestRoundTrip() {
	// Create and run an SQS fetcher
	queueARN, err := QueueURLtoARN(suite.QueueURL)
	assert.Nil(suite.T(), err)

	fetcher, err := NewSQSFetcher(queueARN)
	assert.Nil(suite.T(), err)

	fetcher.NumWorkers = 10
	err = fetcher.Start()
	assert.Nil(suite.T(), err)
	defer fetcher.Stop()

	// Put a message in the queue
	msg := &qrepeater.Message{
		Body: "hola",
	}
	transport := SQSFeederTransport{
		QueueURL: suite.QueueURL,
	}
	err = transport.PutMessage(msg)
	assert.Nil(suite.T(), err)
	if err != nil {
		return
	}

	// Inspect the message SQS fetcher fetched
	found := false

	select {
	case m := <-fetcher.MsgC:
		if m.Body == "hola" {
			found = true
			break
		}
	case <-time.After(time.Second * 5):
		break
	}
	assert.True(suite.T(), found)
}

func (suite *SQSFetcherTestSuite) TearDownTest() {
	var err error
	if suite.QueueURL != "" {
		err = DeleteTempSQSQueue(suite.QueueURL)
	}
	assert.Nil(suite.T(), err)
}

func TestSQSFetcherTestSuite(t *testing.T) {
	suite.Run(t, new(SQSFetcherTestSuite))
}
