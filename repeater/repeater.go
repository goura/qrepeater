package repeater

import (
	"fmt"

	"bitbucket.org/goura/qrepeater"
	"bitbucket.org/goura/qrepeater/arn"
	"bitbucket.org/goura/qrepeater/aws"
	"bitbucket.org/goura/qrepeater/filter"
	log "github.com/Sirupsen/logrus"
)

// Repeater represents a repeater which repeats a message
// from InputQueue to OutputQueue.
// NumFeeders and NumFetchers define how many workers to set up
// for the InputQueue and the OutputQueue.
// FilterChain is an array of filter.Filter, which
// will be applied to the message in order, before sending the
// message to OutputQueue.
type Repeater struct {
	InputQueue  string
	OutputQueue string
	NumFeeders  int
	NumFetchers int
	FilterChain []filter.Filter
	feeder      *qrepeater.Feeder
	fetcher     *qrepeater.Fetcher
	stopC       chan interface{}
}

// Start starts the Repeater.
func (r *Repeater) Start() error {
	inputArn, err := arn.Parse(r.InputQueue)
	if err != nil {
		return err
	}
	outputArn, err := arn.Parse(r.OutputQueue)
	if err != nil {
		return err
	}

	switch inputArn.Service {
	case "sqs":
		r.fetcher, err = aws.NewSQSFetcher(r.InputQueue)
		if err != nil {
			return err
		}
		break
	default:
		return fmt.Errorf("Invalid input ARN: %s", inputArn)
	}

	switch outputArn.Service {
	case "sqs":
		r.feeder, err = aws.NewSQSFeeder(r.OutputQueue)
		if err != nil {
			return err
		}
		break
	case "sns":
		r.feeder, err = aws.NewSNSFeeder(r.OutputQueue)
		if err != nil {
			return err
		}
		break
	default:
		return fmt.Errorf("Invalid output ARN: %s", outputArn)
	}

	r.fetcher.NumWorkers = r.NumFetchers
	r.feeder.NumWorkers = r.NumFeeders

	err = r.fetcher.Start()
	if err != nil {
		return err
	}

	err = r.feeder.Start()
	if err != nil {
		defer r.fetcher.Stop()
		return err
	}

	r.stopC = make(chan interface{})

	// Read from fetcher and pass it to feeder
	// until stopC closes
	go RepeatMsg(r.fetcher.MsgC, r.feeder.MsgC, r.FilterChain, r.stopC)

	return nil
}

// Stop stops the Repeater.
func (r *Repeater) Stop() error {
	close(r.stopC)

	err := r.fetcher.Stop()
	if err != nil {
		return err
	}
	err = r.feeder.Stop()
	if err != nil {
		return err
	}
	return nil
}

func RepeatMsg(fromMsgC, toMsgC chan *qrepeater.Message, filterChain []filter.Filter, stopC chan interface{}) {
	// Read from fromMsgC and pass it to toMsgC until stopC closes.
	// stopC is used to notify this function to end from outside.
	for {
		select {
		// Stop, or retrieve a message
		case <-stopC:
			return
		case m := <-fromMsgC:
			// Pass m through the filterChain
			for _, f := range filterChain {
				// Apply this filter to the message
				newM, err := f.FilterFunc(m)
				if err != nil {
					// Filter failed to process the message.
					// Log this incident and escape the chain.
					log.WithFields(log.Fields{
						"component":     "filter",
						"action":        "filter_message",
						"filter":        f.Name,
						"finish_handle": m.FinishHandle,
						"error":         err.Error(),
					}).Error("filter_message failed, this message won't be processed")
					break
				}
				m = newM
			}

			// Filter returns nil if the filter decides not to
			// forward the message.
			// In such cases we skip this message.
			if m == nil {
				continue
			}

			select {
			// Stop, or forward the message to toMsgC
			case <-stopC:
				return
			case toMsgC <- m:
				continue
			}
		}
	}
}
