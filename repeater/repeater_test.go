// +build integration

package repeater

import (
	"fmt"
	"math/rand"
	"net/url"
	"strings"
	"testing"
	"time"

	"bitbucket.org/goura/qrepeater"
	"bitbucket.org/goura/qrepeater/filter"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// CreateTempSQSQueue creates a temporary SQS queue and returns its URL.
func CreateTempSQSQueue() (string, error) {
	svc := sqs.New(nil)

	unixtime := time.Now().Unix()
	randNum := rand.Uint32()
	queueName := fmt.Sprintf("test-%d-%d", unixtime, randNum)
	params := &sqs.CreateQueueInput{
		QueueName: aws.String(queueName),
	}

	resp, err := svc.CreateQueue(params)

	if err != nil {
		return "", err
	}
	return *resp.QueueURL, nil
}

// DeleteTempSQSQueue deletes an SQS queue corresponding the given URL.
func DeleteTempSQSQueue(QueueURL string) error {
	svc := sqs.New(nil)

	params := &sqs.DeleteQueueInput{
		QueueURL: aws.String(QueueURL),
	}

	_, err := svc.DeleteQueue(params)
	return err
}

// QueueURLtoARN converts an SQS queue URL to an SQS queue ARN
// only by text matching.
func QueueURLtoARN(queueURL string) (string, error) {
	// from | https://sqs.ap-northeast-1.amazonaws.com/12345678901/aaaa
	// to   | arn:aws:sqs:ap-northeast-1:12345678901:aaaa
	u, err := url.Parse(queueURL)
	if err != nil {
		return "", err
	}

	fields := strings.Split(u.Host, ".")
	if len(fields) < 3 {
		return "", fmt.Errorf("Wrong SQS queue URL: %s", queueURL)
	}
	region := fields[1]

	fields = strings.Split(u.Path, "/")
	if len(fields) != 3 {
		return "", fmt.Errorf("Wrong SQS queue URL: %s", queueURL)
	}
	account := fields[1]
	resource := fields[2]

	arn := fmt.Sprintf("arn:aws:sqs:%s:%s:%s", region, account, resource)
	return arn, nil
}

func TestQueueURLtoARN(t *testing.T) {
	s1 := "https://sqs.ap-northeast-1.amazonaws.com/12345678901/aaaa"
	s2 := "arn:aws:sqs:ap-northeast-1:12345678901:aaaa"
	result, err := QueueURLtoARN(s1)
	assert.Nil(t, err)
	assert.Equal(t, s2, result)
}

type RepeaterTestSuiteSQS struct {
	suite.Suite
	InputQueueURL  string
	OutputQueueURL string
}

func (suite *RepeaterTestSuiteSQS) SetupTest() {
	var err error
	suite.InputQueueURL, err = CreateTempSQSQueue()
	assert.Nil(suite.T(), err)
	suite.T().Logf("InputQueueURL: %s", suite.InputQueueURL)

	suite.OutputQueueURL, err = CreateTempSQSQueue()
	assert.Nil(suite.T(), err)
	suite.T().Logf("OutputQueueURL: %s", suite.OutputQueueURL)
}

func (suite *RepeaterTestSuiteSQS) TearDownTest() {
	var err error
	if suite.InputQueueURL != "" {
		err = DeleteTempSQSQueue(suite.InputQueueURL)
		assert.Nil(suite.T(), err)
	}

	if suite.OutputQueueURL != "" {
		err = DeleteTempSQSQueue(suite.OutputQueueURL)
		assert.Nil(suite.T(), err)
	}
}

// Test with no filters
func (suite *RepeaterTestSuiteSQS) TestRepeaterWithNoFilters() {
	suite.testRepeater([]filter.Filter{}, 100, 100)
}

// Test with some filters
func (suite *RepeaterTestSuiteSQS) TestRepeaterWithFilters() {
	// Add "-foo" to the body
	fooFilterFunc := func(m *qrepeater.Message) (*qrepeater.Message, error) {
		m.Body += "-foo"
		return m, nil
	}
	// Add "-bar" to the body
	barFilterFunc := func(m *qrepeater.Message) (*qrepeater.Message, error) {
		m.Body += "-bar"
		return m, nil
	}

	// If the number for a message contains "9", the message will be discarded
	discardNineFilterFunc := func(m *qrepeater.Message) (*qrepeater.Message, error) {
		// m.Body should be like "hello-123-foo-bar"
		fields := strings.Split(m.Body, "-")
		index := strings.Index(fields[1], "9")
		if index > -1 {
			m.Finish()
			return nil, nil
		}

		return m, nil
	}

	filterChain := []filter.Filter{
		filter.Filter{
			FilterFunc: fooFilterFunc,
			Name:       "fooFilter",
		},
		filter.Filter{
			FilterFunc: barFilterFunc,
			Name:       "barFilter",
		},
		filter.Filter{
			FilterFunc: discardNineFilterFunc,
			Name:       "discardNineFilter",
		},
	}
	suite.testRepeater(filterChain, 100, 81)
}

// Create source and destination queue, fill the source with messages,
// run the repeater for a while, and see if it's successfully transferred.
func (suite *RepeaterTestSuiteSQS) testRepeater(filterChain []filter.Filter, numOfMessages, expectNumber int) {
	// Put numOfMessages messages into the input queue
	svc := sqs.New(nil)
	for i := 0; i < numOfMessages; i++ {
		params := &sqs.SendMessageInput{
			MessageBody: aws.String(fmt.Sprintf("hello-%d", i)),
			QueueURL:    aws.String(suite.InputQueueURL),
		}
		_, err := svc.SendMessage(params)
		assert.Nil(suite.T(), err)
	}

	// Set up the repeater
	inputARN, err := QueueURLtoARN(suite.InputQueueURL)
	assert.Nil(suite.T(), err)

	outputARN, err := QueueURLtoARN(suite.OutputQueueURL)
	assert.Nil(suite.T(), err)

	r := Repeater{
		InputQueue:  inputARN,
		OutputQueue: outputARN,
		NumFeeders:  20,
		NumFetchers: 5,
		FilterChain: filterChain,
	}

	suite.T().Logf("Run a repeater for a while...")
	err = r.Start()
	assert.Nil(suite.T(), err)

	// Check if the input contains 0 messages and
	// the destination contains numOfMessages messages
	flag := false
	for i := 0; i < 20; i++ {
		numMsg1 := queueStatus(suite.InputQueueURL)
		numMsg2 := queueStatus(suite.OutputQueueURL)
		suite.T().Logf("from -> to: %s -> %s\n", numMsg1, numMsg2)

		if numMsg1 == "0" && numMsg2 == fmt.Sprintf("%d", expectNumber) {
			flag = true
			break
		}
		time.Sleep(time.Second * 5)
	}
	assert.True(suite.T(), flag) // OutputQueue messages should be expectNumber

	err = r.Stop()
	assert.Nil(suite.T(), err)
	suite.T().Logf("Stopped the repeater")
}

func queueStatus(qurl string) string {
	svc := sqs.New(nil)
	s := "All"
	params := &sqs.GetQueueAttributesInput{
		AttributeNames: []*string{&s},
		QueueURL:       aws.String(qurl),
	}
	resp, err := svc.GetQueueAttributes(params)
	if err != nil {
		return ""
	}
	numMsg, ok := resp.Attributes["ApproximateNumberOfMessages"]
	if !ok {
		return ""
	}
	return *numMsg
}

func TestRepeaterTestSuite(t *testing.T) {
	suite.Run(t, new(RepeaterTestSuiteSQS))
}
