package repeater

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"bitbucket.org/goura/qrepeater/filter"
)

// RunRepeater runs a repeater, specified by command line flags.
// It waits for SIGINT and SIGTERM, and gracefully stops the repeater
// when those signals are received.
func RunRepeater() error {
	repeater, err := parseFlags()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return fmt.Errorf("")
	}

	err = repeater.Start()
	if err != nil {
		return err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)

	<-ch
	fmt.Fprintf(os.Stderr, "Signal received, waiting for repeaters to stop...")
	err = repeater.Stop()
	if err != nil {
		return err
	}

	return nil
}

// parseFlags parses the command line option
// and generates a Repeater instance.
func parseFlags() (*Repeater, error) {
	var inputQueue string
	var outputQueue string
	var numFetchers int
	var numFeeders int
	var filterChainStr string

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.StringVar(&inputQueue, "in", "", "REQUIRED: ARN of the queue to fetch messages (SQS)")
	flag.StringVar(&outputQueue, "out", "", "REQUIRED: ARN of the queue to feed messages (SQS/SNS)")
	flag.StringVar(&filterChainStr, "filters", "", "Filters to use. Separated multiple filters by colon.")
	flag.IntVar(&numFetchers, "numFetcher", 1, "Number of workers to fetch messages")
	flag.IntVar(&numFeeders, "numFeeder", 1, "Number of workers to fetch messages")

	flag.Parse()

	if inputQueue == "" || outputQueue == "" {
		flag.Usage()
		return nil, fmt.Errorf("inputQueue/outputQueue must be specified")
	}

	r := regexp.MustCompile("^arn:aws:sqs:")
	if !r.Match([]byte(inputQueue)) {
		flag.Usage()
		return nil, fmt.Errorf("`in` currently supports only SQS")
	}

	r = regexp.MustCompile("^arn:aws:(sqs|sns):")
	if !r.Match([]byte(outputQueue)) {
		flag.Usage()
		return nil, fmt.Errorf("`out` currently supports only SQS or SNS")
	}

	filterChain := []filter.Filter{}
	for _, s := range strings.Split(filterChainStr, ":") {
		f, ok := filter.Lookup(s)
		if !ok {
			return nil, fmt.Errorf("Filter named `%s` could not be found", s)
		}
		filterChain = append(filterChain, f)
	}

	repeater := &Repeater{
		InputQueue:  inputQueue,
		OutputQueue: outputQueue,
		NumFetchers: numFetchers,
		NumFeeders:  numFeeders,
		FilterChain: filterChain,
	}

	return repeater, nil
}
